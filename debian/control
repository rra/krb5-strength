Source: krb5-strength
Section: net
Priority: optional
Maintainer: Russ Allbery <rra@debian.org>
Build-Depends:
 cracklib-runtime <!nocheck>,
 debhelper-compat (= 13),
 libcdb-dev,
 libconst-fast-perl,
 libcrack2-dev,
 libcrypt-pbkdf2-perl <!nocheck>,
 libdb-file-lock-perl <!nocheck>,
 libdbd-sqlite3-perl,
 libdbi-perl,
 libgetopt-long-descriptive-perl <!nocheck>,
 libipc-run-perl <!nocheck>,
 libjson-maybexs-perl,
 libkrb5-dev (>= 1.9),
 libperl6-slurp-perl,
 libsqlite3-dev,
 libtest-minimumversion-perl <!nocheck>,
 libtest-pod-perl <!nocheck>,
 libtest-strict-perl <!nocheck>,
 perl,
 pkg-config,
 tinycdb,
Rules-Requires-Root: no
Standards-Version: 4.6.2
Homepage: https://www.eyrie.org/~eagle/software/krb5-strength/
Vcs-Git: https://salsa.debian.org/rra/krb5-strength.git
Vcs-Browser: https://salsa.debian.org/rra/krb5-strength

Package: krb5-strength
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends},
Depends:
 adduser,
 ${misc:Depends},
 ${perl:Depends},
 ${shlibs:Depends},
Recommends:
 cracklib-runtime,
 libconst-fast-perl,
 libcrypt-pbkdf2-perl,
 libdb-file-lock-perl,
 libdbd-sqlite3-perl,
 libdbi-perl,
 libgetopt-long-descriptive-perl,
 libipc-run-perl,
 libjson-maybexs-perl,
 tinycdb,
Enhances:
 heimdal-kdc,
 krb5-admin-server,
Description: Password strength checking for Kerberos KDCs
 krb5-strength provides a password quality plugin for the MIT Kerberos KDC
 (specifically the kadmind server), an external password quality program
 for use with Heimdal, and a per-principal password history implementation
 for Heimdal.  Passwords can be tested with CrackLib, checked against a
 CDB or SQLite database of known weak passwords with some transformations,
 checked for length, checked for non-printable or non-ASCII characters
 that may be difficult to enter reproducibly, required to contain
 particular character classes, or any combination of these tests.
 .
 No dictionary is shipped with this package.  A CrackLib dictionary can be
 created with the tools in cracklib-runtime, a CDB or SQLite database can
 be created from a password list (obtained separately) using the tools
 included in this package, or both.
 .
 The recommended packages are needed to generate CDB or SQLite databases
 and for the password history implementation for Heimdal.

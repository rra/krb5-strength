krb5-strength (3.3-1) unstable; urgency=medium

  * New upstream release.
    - Increase hash iterations for heimdal-history.
    - Explicitly clear another password before freeing.

 -- Russ Allbery <rra@debian.org>  Mon, 25 Dec 2023 19:12:56 -0800

krb5-strength (3.2-4) unstable; urgency=medium

  * Run make maintainer-clean during debian/rules clean, since
    debian/rules build runs ./bootstrap and thus regenerates test files.
    (Closes: #1048564)
  * Update to standards version 4.6.2 (no changes required).
  * Refresh upstream signing key.

 -- Russ Allbery <rra@debian.org>  Sun, 22 Oct 2023 12:37:47 -0700

krb5-strength (3.2-3) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTBFS with nocheck profile. (Closes: #983701)

 -- Russ Allbery <rra@debian.org>  Sun, 28 Feb 2021 11:27:30 -0800

krb5-strength (3.2-2) unstable; urgency=medium

  * Mark build dependencies used only for the test suite with <!nocheck>.
    Thanks, Helmut Grohne.  (Closes: #978723)
  * Remove Build-Dependency on libfile-slurp-perl, which was not used.
  * Add debian/upstream/metadata file.
  * Change the Debian packaging branch to debian/unstable.
  * Change canonical packaging repository to Salsa.
  * Update to standards version 4.5.1 (no changes required).
  * Refresh upstream signing key.

 -- Russ Allbery <rra@debian.org>  Wed, 30 Dec 2020 18:11:26 -0800

krb5-strength (3.2-1) unstable; urgency=medium

  * New upstream release.
    - New --check-only option to heimdal-history.
    - Increase hash iterations for heimdal-history for new hashes.
    - Use explicit_bzero to overwrite copies of passwords.
  * Drop upstream patch to modify the test suite, since the upstream test
    suite now correctly handles a system CrackLib library.
  * Include the full text of the license of the embedded copy of cracklib
    (not used in the Debian build) in debian/copyright, since it's not
    identical to the Artistic license included in common-licenses.
  * Update to debhelper compatibility level V13.
  * Update to standards version 4.5.0 (no changes required).
  * Refresh upstream signing key.

 -- Russ Allbery <rra@debian.org>  Sun, 17 May 2020 10:27:44 -0700

krb5-strength (3.1-2) unstable; urgency=medium

  * Update standards version to 4.2.1.
    - Enable verbose test output.
    - Install the upstream release notes as NEWS.gz, not changelog.gz.
    - Add Rules-Requires-Root: no.
    - Use https for URLs in debian/copyright.
    - Change priority to optional.
  * Update to debhelper compatibility levl V11.
  * Bump debian/watch version to 4 and use https.
  * Add upstream-vcs-tag configuration to debian/gbp.conf.
  * Remove obsolete debian/source/options that was forcing the compression
    format to xz (now the default).
  * Refresh upstream signing key.

 -- Russ Allbery <rra@debian.org>  Fri, 31 Aug 2018 17:07:43 -0700

krb5-strength (3.1-1) unstable; urgency=medium

  * New upstream release.
    - New configuration option cracklib_maxlen.
    - require_classes now supports requiring a minimum number of classes.
    - Capitalize the first letter of error messages.
    - Apply SuSE patch for a security vulnerability in the embedded
      CrackLib.  This didn't affect the way it was used in this package,
      but best to be sure anyway.
    - Configuration instructions are now provided in installed man pages,
      including a new krb5-strength man page, to make them more accessible
      after installation.
  * Patch the upstream test suite to change the expected results for a few
    passwords that are rejected by the embedded CrackLib but accepted by
    the system CrackLib (which the Debian package is built with).
  * Re-enable treating test suite failures as package build failures now
    that bug #724570 in CrackLib was fixed in Debian.
  * Switch to the DEP-14 branch layout and update debian/gbp.conf and
    Vcs-Git accordingly.
  * Prefer *.tar.xz in debian/watch to match packaging.
  * Fix Upstream-Contact email address in debian/copyright.
  * Switch to https for all package URLs.
  * Run wrap-and-sort -ast.
  * Refresh upstream signing key.
  * Enable all hardening flags.
  * Update to debhelper compatibility level V10.
    - Remove explicit dh-autoreconf dependency and invocation.
    - Remove explicit --parallel flags.
  * Update standards version to 3.9.8 (no changes required).

 -- Russ Allbery <rra@debian.org>  Sun, 25 Dec 2016 12:40:41 -0800

krb5-strength (3.0-1) unstable; urgency=medium

  * New upstream release.
    - SQLite password dictionaries are now supported and can be used to
      reject passwords within edit distance one of any dictionary word.
    - cdbmake-wordlist has been renamed to krb5-strength-wordlist and can
      also generate SQLite databases compatible with this plugin and
      Heimdal quality check program.
    - heimdal-history, a password history implementation for Heimdal, has
      been added and can be stacked with heimdal-strength to check both
      history and password strength.
    - New configuration option, minimum_different, which sets the minimum
      number of different characters required in a password.
  * Add the upstream signing key to debian/upstream/signing-key.asc and
    configure uscan to do signature validation.  Configure uscan to
    download the xz tarball instead of the gz tarball.
  * Create a _history user and group and a /var/lib/heimdal-history
    directory on package installation for the use of heimdal-history,
    remove the user and the standard database on purge, and remove the
    directory if empty on package purge or removal.

 -- Russ Allbery <rra@debian.org>  Wed, 26 Mar 2014 00:04:13 -0700

krb5-strength (2.2-1) unstable; urgency=low

  * New upstream release.
    - Support for more complex length-sensitive character class
      restrictions using the new require_classes configuration setting.
    - cdbmake-wordlist now supports filtering out words based on maximum
      length and user-supplied regular expressions, and supports running
      in filter mode to generate a new word list.
  * Update to standards version 3.9.5 (no changes required).

 -- Russ Allbery <rra@debian.org>  Mon, 16 Dec 2013 15:36:29 -0800

krb5-strength (2.1-1) unstable; urgency=low

  * New upstream release.
    - Improve some of the password rejection error messages.

 -- Russ Allbery <rra@debian.org>  Thu, 10 Oct 2013 17:09:37 -0700

krb5-strength (2.0-1) unstable; urgency=low

  * Initial upload to Debian.  (Closes: #725753)
  * New upstream release.
    - Add native support for the MIT Kerberos password quality plugin
      interface included in MIT Kerberos 1.9 and later.
    - Stop building the Heimdal plugin.  Heimdal prefers using an external
      check program anyway.
    - Add support for TinyCDB dictionaries with a simpler dictionary
      lookup algorithm.  This allows use of this package to check
      passwords against a large, fast dictionary with a minimum of
      permutations as an alternative or supplement to the extensive
      permutations tested by CrackLib.
    - Minimum password length can now be enforced directly through
      configuration of this module without relying on CrackLib.
    - New boolean settings require_ascii_printable and require_non_letter
      to reject passwords with non-ASCII or non-printable characters and
      to require passwords contain at least one non-letter (and
      non-space).
    - The plugin and external checking program will now run without a
      dictionary configured so that they can be used only to check length
      and the lighter character restrictions if so desired.
    - When checking for passwords based on the principal, also check each
      component of the principal to find passwords based on the realm.
  * Eliminate the heimdal-strength package.  krb5-strength now builds a
    single binary package of the same name including the MIT plugin and
    the Heimdal external password quality program.  The Heimdal plugin is
    not built by the Debian packaging because Heimdal prefers external
    programs.  The plugin can be added later as a separate package if
    there is demand.
  * Revise the package long description for the merger of krb5-strength
    and heimdal-strength and the new capabilities in 2.0.
  * Recommend cracklib-runtime and tinycdb since they are required to
    build dictionaries.  Downgrade krb5-admin-server to Enhances and add
    heimdal-kdc.
  * Update debhelper compatibility level to V9.
    - Enable hardening flags, including bindnow and PIE.
    - Enable parallel builds.
  * Use dh-autoreconf to rebuild the build system during package builds.
  * Use xz compression for the upstream and Debian tarballs.
  * Add branch information to the Vcs-Git metadata.
  * Update standards version to 3.9.4 (no changes required).
  * Rewrite debian/copyright in copyright-format 1.0.
  * Remove README.Debian.  All of that information is now available in the
    installed upstream README file.
  * Remove Bugs header now that this package is in Debian proper.

 -- Russ Allbery <rra@debian.org>  Mon, 07 Oct 2013 18:56:49 -0700

krb5-strength (1.1-1) unstable; urgency=low

  * New upstream release.
    - Increase minimum password length to 8.
    - Reject passwords formed from the username with digits appended.
    - Reject duplicated dictionary words.
    - Fix variable sizes on 64-bit platforms.
  * Add a Bugs header directing bug reports to me personally.
  * Update to debhelper compatibility level V8.
  * Switch to Debian source package format 3.0 (quilt) with a custom local
    patch header.
  * Update standards version to 3.9.3 (no changes required).

 -- Russ Allbery <rra@debian.org>  Fri, 11 May 2012 15:03:11 -0700

krb5-strength (1.0-1) unstable; urgency=low

  * New upstream release.
    - Add an external password strength program for Heimdal.
    - Add a plugin for the Heimdal password strength support.
  * Create a separate heimdal-strength package containing only the
    external password check program for Heimdal.  The Heimdal version of
    the libkadm5srv plugin isn't packaged for the time being.
  * Update debhelper compatibility mode to V7.
    - Use debhelper rule minimization with overrides.
    - Add ${misc:Depends} to dependencies.
  * Add Homepage, Vcs-Git, and Vcs-Browser control fields.
  * Add a watch file.
  * Update standards version to 3.8.4 (no changes required).

 -- Russ Allbery <rra@debian.org>  Tue, 16 Feb 2010 22:47:35 -0800

krb5-strength (0.5-1) unstable; urgency=low

  * New upstream release.
    - More checks for passwords based on the principal.

 -- Russ Allbery <rra@debian.org>  Wed, 18 Jul 2007 23:16:37 -0700

krb5-strength (0.3-1) unstable; urgency=low

  * New upstream release with a different name.
    - Many cleanups to the code and build system.
    - Unnecessary differences from CrackLib removed.
    - Some Debian CrackLib patches applied for robustness.
  * Updated README.Debian with a better example kdc.conf entry.
  * No longer install the packer binary.  We can use the one from
    cracklib-runtime.

 -- Russ Allbery <rra@debian.org>  Fri, 23 Mar 2007 15:27:50 -0700

krb5-passwd-strength (0.2-1) unstable; urgency=low

  * Initial release.

 -- Russ Allbery <rra@debian.org>  Thu, 22 Jun 2006 15:07:03 -0700
